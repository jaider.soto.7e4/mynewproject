﻿# Practica GIT
Vamos a probar a trabajar dos personas en un mismo repositorio. Yo voy a ser el miembro 2 y Jaider será el miembro 1.
Primero ambos haremos un clone:
```bash
git clone https://gitlab.com/jaider.soto.7e4/mynewproject
```

Ahora Jaider creará un archivo equip.txt y escribirá "En este documento guardaremos el nombre de los miembros". Acto seguido hará un commit y lo subirá al repositorio remoto:
```bash
touch equip.txt
echo "En este documento guardaremos el nombre de los miembros" > equip.txt
```

Ahora actualizo mi repositorio local para tener este archivo:
```bash
git pull
```

Una vez hecho esto lo que vamos a probar es a modificar los dos el archivo a la vez (Cada uno añadirá su nombre). Primero Jaider lo subirá y luego lo subiré yo. Cuando yo intento subirlo:
```
git push
Username for 'https://gitlab.com': jonas.munyoz.7e4@itb.cat
Password for 'https://jonas.munyoz.7e4@itb.cat@gitlab.com': 
warning: s'està redirigint a https://gitlab.com/jaider.soto.7e4/mynewproject.git/
To https://gitlab.com/jaider.soto.7e4/mynewproject
 ! [rejected]        main -> main (fetch first)
error: s'ha produït un error en pujar algunes referències a «https://gitlab.com/jaider.soto.7e4/mynewproject»
pista: S'han rebutjat les actualitzacions perquè el remot conté canvis
pista: que no teniu localment. Això acostuma a ser causat per un altre dipòsit
pista: que ha pujat a la mateixa referència. Pot ser que primer vulgueu
pista: integrar els canvis remots (per exemple, 'git pull ...') abans de
pista: pujar de nou.
pista: Vegeu la 'Nota sobre avanços ràpids' a 'git push --help' per detalls.
sjo@hA209-34-WLD-034:~/Escriptori/DADES/JonasM/git/mynewproject$ 
```

Ahora vamos a hacer un pull y veremos un mensaje de warning:
```
git pull
Username for 'https://gitlab.com': jonas.munyoz.7e4@itb.cat
Password for 'https://jonas.munyoz.7e4@itb.cat@gitlab.com': 
warning: s'està redirigint a https://gitlab.com/jaider.soto.7e4/mynewproject.git/
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
S'estan desempaquetant els objectes: 100% (3/3), 301 bytes | 301.00 KiB/s, fet.
De https://gitlab.com/jaider.soto.7e4/mynewproject
   ac4d175..365716b  main       -> origin/main
S'està autofusionant equip.txt
CONFLICTE (contingut): Conflicte de fusió en equip.txt
La fusió automàtica ha fallat; arregleu els conflictes i després cometeu el resultat.
```

Si nos fijamos en el fichero equip.txt veremos que hay unas lineas que no hemos escrito. Estas lineas nos indican que partes del documento han entrado en conflicto:
```
En aquest document desarem el nom dels membres
<<<<<<< HEAD
Jonas Muñoz Pérez
=======
Jaider Mateo Soto Granada
>>>>>>> 365716bb248513d3f76c14806954d3619957f62b
```

Resolvemos el problema:
```
En aquest document desarem el nom dels membres
Jaider Mateo Soto Granada
```

Y subimos al repositorio remoto:
```bash
git add .
git commit -m "Added new line to equip.txt"
git push
```

Y ahora si nos deja, problema resuelto.
